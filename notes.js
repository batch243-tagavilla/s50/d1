/* 
    Syntax: npx create-react-app "project-name"
    
    After creating project, execute the command "npm start" to run our project in our localhost
    
    Delete the unnecessary files from the newly created project
    1. App.test.js
    2. index.css
    3. logo.svg
    4. reportWebVitals.js

    After we deleted the unnecessary files, we encountered errors
    Errors encountered: imporation of the deleted files
    To fix, we remove the syntax or codes of the imported files

    Now, we have a blank slate where we can start building our own ReactJS app
*/

/* 
    Similar to NodeJS and expressJS, we can install packages in our react application to make the work easier for us.
    example: npm install react-bootstrap bootstrap

    The "import"  statement allows us to use the code/exported modules from files similar to how we use the require keyword in NodeJS

    ReactJS components are independent, reusable  pieces of code which is normall constans JS and JSX syntax which make up a a part of our application
*/

/* 
    Storing information in a context object is done by providing the information using the corresponding "Provider" component and passing the information via the "value" prop

    All information provided to the Provider component can be accessed later on from the context object as properties
*/


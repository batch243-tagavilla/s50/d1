import React from "react";

// Create a context object
/* 
    - A context object as the name state is a data type that can used to store information that can be shared to other components within the app
    - the context object is a different approach to passing information between components and allows easier access by the use of prop-drilling
*/

// CreateContext is use to create a context in react
const UserContext = React.createContext();

// The "Provider" component allows other component to consume/use the context object and supply the necessary information needed to the context Object
export const UserProvider = UserContext.Provider;

export default UserContext;
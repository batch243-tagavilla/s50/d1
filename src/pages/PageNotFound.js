import { Card, Col, Container, Row } from "react-bootstrap";

export default function PageNotFound() {
    return (
        <Container>
            <Row>
                <Col xs={12} md={4} className="offset-md-4 offset-0">
                    <Card>
                        <Card.Body className="text-center">
                            <Card.Subtitle className="mb-3">Zuitt Booking:</Card.Subtitle>
                            <Card.Title className="fw-bold mb-3 fs-2">Page Not Found</Card.Title>
                            <Card.Text>Go back to the <a href="/">homepage</a>.</Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
}

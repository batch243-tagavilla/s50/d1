import { useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import CourseCard from "../components/CourseCard.js";
// import courseData from "../data/courses.js";

export default function Courses() {
    const [courses, setCourses] = useState([]);
    // const local = localStorage.getItem("email")
    // console.log(local);
    useEffect(() => {
        fetch(`${process.env.REACT_APP_URI}/courses/allActiveCourses`)
            .then((response) => response.json())
            .then((data) => {
                console.log(data);

                // Sets the "courses" state to map the data retrieved from the fetch request into several "CourseCard" components
                setCourses(
                    data.map((course) => {
                        return (
                            <CourseCard courseProp={course} key={course._id} />
                        );
                    })
                );
            });
    }, []);

    // const courses = courseData;

    return <Container>{courses}</Container>;
}

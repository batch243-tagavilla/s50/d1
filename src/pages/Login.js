import { Row, Col, Container, Button, Form } from "react-bootstrap";
import { useEffect, useState, useContext } from "react";

// Importing sweetalert2
import Swal from "sweetalert2";

import UserContext from "../UserContext";

export default function Login() {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [isDisabled, setIsDisabled] = useState(true);

    // allows us to consume the UserContext object and its properties to use for user validation
    const { user, setUser } = useContext(UserContext);

    useEffect(() => {
        if (email && password) {
            setIsDisabled(false);
        } else {
            setIsDisabled(true);
        }
    }, [email, password]);

    function loginUser(e) {
        e.preventDefault();
        /* alert(`Logged-in successfully`);
        

        // Storing information in the local storage will make the data persistent even as the page is refreshed unlike with the use of states where informations is reset when refreshing the page
        localStorage.setItem("email", email);

        // set the global user state to have properties obtained from the local storage
        // though access to the user information can be done via the localStorage. this is necessary to update the user sate which will help u[date the App component and re-rend it to avoid refreshing the page upon user login and logout.
        setUser(localStorage.getItem("email"));
        setEmail("");
        setPassword(""); */

        // Process wherein it will fetch a request to the corresponding API
        // The header information "Content-Type" is use to specify that the information being send to the backend will be sent in the form of json
        // The fetch request will communicate with our backend application providing it with a stringified JSON
        /* 
            Syntax:
                fetch("url"), {options: method, headers body}
                .then (response => res.json())
                .then(data => {data process})
        */

        fetch(`${process.env.REACT_APP_URI}/users/login`, {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({
                email: email,
                password: password,
            }),
        })
            .then((response) => response.json())
            .then((data) => {
                console.log(data);
                if (data.accessToken !== `empty`) {
                    localStorage.setItem("token", data.accessToken);
                    retrieveUserDetails(data.accessToken);
                    Swal.fire({
                        title: "Login Successful",
                        icon: "success",
                        text: "Welcome to our website",
                    });
                } else {
                    Swal.fire({
                        title: "Authentication Failed",
                        icon: "error",
                        text: "Check your login details and try again", 
                    })
                    setPassword("");
                }
            });

        const retrieveUserDetails = (token) => {
            fetch(`${process.env.REACT_APP_URI}/users/profile`, {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            })
                .then((response) => response.json())
                .then((data) => {
                    console.log(data);
                    setUser({ id: data._id, isAdmin: data.isAdmin });
                });
        };
    }

    return (
        <Container>
            <Row>
                <Col className="col-md-4 col-8 offset-md-4 offset-2">
                    <Form
                        className="text-white bg-secondary p-3"
                        onSubmit={loginUser}
                    >
                        <Form.Group className="mb-3" controlId="email">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control
                                type="email"
                                placeholder="Enter email"
                                value={email}
                                onChange={(event) => {
                                    setEmail(event.target.value);
                                }}
                                required
                            />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="password">
                            <Form.Label>Password</Form.Label>
                            <Form.Control
                                type="password"
                                placeholder="Enter your password"
                                value={password}
                                onChange={(event) => {
                                    setPassword(event.target.value);
                                }}
                                required
                            />
                        </Form.Group>

                        <Button
                            variant="primary"
                            type="submit"
                            disabled={isDisabled}
                        >
                            Login
                        </Button>
                    </Form>
                </Col>
            </Row>
        </Container>
    );
}

import { Navigate } from "react-router-dom";
import { useContext, useEffect } from "react";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function Logout() {
    const { user, setUser, unSetUser } = useContext(UserContext);
    unSetUser();
    // setUser(localStorage.getItem("email"));

    useEffect(() => {
        // if (!user.id) {
        setUser({ id: null, isAdmin: false });
        // alert(`Log-out successfully!`);
        // }
    }, []);

    Swal.fire({
        title: "Welcome back to the outside world",
        icon: "info",
        text: "Babalik ka rin",
    });

    return <Navigate to="/login" />;
}

import { Row, Col, Container, Button, Form } from "react-bootstrap";
import { useEffect, useState, useContext } from "react";
import Swal from "sweetalert2";
import { useNavigate } from "react-router-dom";

export default function Register() {
    const [email, setEmail] = useState("");
    const [password1, setPassword1] = useState("");
    const [password2, setPassword2] = useState("");
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [mobileNo, setMobileNo] = useState("");
    const [isActive, setIsActive] = useState(false);
    const redirect = useNavigate();

    useEffect(() => {
        if (
            email &&
            password1 &&
            password2 &&
            firstName &&
            lastName &&
            mobileNo &&
            password1 === password2
        ) {
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [email, password1, password2, firstName, lastName, mobileNo]);

    // this function will be triggered when the inputs in the Form will be submitted
    function registerUser(event) {
        event.preventDefault();

        const verifyEmail = () => {
            fetch(`${process.env.REACT_APP_URI}/users/checkEmail`, {
                method: `POST`,
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify({
                    email: email,
                }),
            })
                .then((response) => response.json())
                .then((data) => {
                    if (data) {
                        Swal.fire({
                            title: "Duplicate email found",
                            icon: "error",
                            text: "Please provide a different email.",
                        });
                    } else {
                        registerAccount();
                    }
                });
        };

        const registerAccount = () => {
            fetch(`${process.env.REACT_APP_URI}/users/register`, {
                method: `POST`,
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify({
                    firstName: firstName,
                    lastName: lastName,
                    email: email,
                    password: password1,
                    mobileNo: mobileNo,
                }),
            })
                .then((response) => response.json())
                .then((data) => {
                    console.log(data);
                    if (data) {
                        Swal.fire({
                            title: "Registration successful",
                            icon: "success",
                            text: "Welcome to Zuitt",
                        });
                        clearInputs();
                        redirect("/login");
                    } 
                });
        };

        verifyEmail();
    }

    function clearInputs() {
        setEmail("");
        setPassword1("");
        setPassword2("");
        setFirstName("");
        setLastName("");
        setMobileNo("");
    }

    return (
        <Container>
            <Row>
                <Col className="col-md-4 col-8 offset-md-4 offset-2">
                    <Form
                        className="text-white bg-secondary p-3"
                        onSubmit={registerUser}
                    >
                        <Form.Group className="mb-3" controlId="email">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control
                                type="email"
                                placeholder="Enter email"
                                value={email}
                                onChange={(event) => {
                                    setEmail(event.target.value);
                                }}
                                required
                            />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="password1">
                            <Form.Label>Password</Form.Label>
                            <Form.Control
                                type="password"
                                placeholder="Enter your desire password"
                                value={password1}
                                onChange={(event) => {
                                    setPassword1(event.target.value);
                                }}
                                required
                            />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="password2">
                            <Form.Label>Repeat Password</Form.Label>
                            <Form.Control
                                type="password"
                                placeholder="Repeat your password"
                                value={password2}
                                onChange={(event) => {
                                    setPassword2(event.target.value);
                                }}
                                required
                            />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="firstName">
                            <Form.Label>First Name</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Enter your first name"
                                value={firstName}
                                onChange={(event) => {
                                    setFirstName(event.target.value);
                                }}
                                required
                            />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="lastName">
                            <Form.Label>Last Name</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Enter your last name"
                                value={lastName}
                                onChange={(event) => {
                                    setLastName(event.target.value);
                                }}
                                required
                            />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="mobileNo">
                            <Form.Label>Mobile No</Form.Label>
                            <Form.Control
                                type="number"
                                placeholder="09XXXXXXXXX"
                                value={mobileNo}
                                onChange={(event) => {
                                    setMobileNo(event.target.value);
                                }}
                                required
                            />
                        </Form.Group>

                        <Button
                            variant="primary"
                            type="submit"
                            disabled={!isActive}
                        >
                            Register
                        </Button>
                    </Form>
                </Col>
            </Row>
        </Container>
    );
}

import "./App.css";

// Pages Import
import AppNavbar from "./components/AppNavbar.js";
import Home from "./pages/Home.js";
import Courses from "./pages/Courses.js";
import CourseView from "./pages/CourseView";
import Register from "./pages/Register.js";
import Login from "./pages/Login.js";
import Logout from "./pages/Logout.js";
import PageNotFound from "./pages/PageNotFound.js";

import {
    Navigate,
    BrowserRouter as Router,
    Routes,
    Route,
} from "react-router-dom";
import { useState, useEffect } from "react";

import { UserProvider } from "./UserContext.js";

function App() {
    // State hook for the user that will be globally accessible using the useContext
    // This will also be used to store the user information and will be used for validating if the user is logged in on the app or not
    // const [user, setUser] = useState(localStorage.getItem("email"));
    const [user, setUser] = useState({ id: null, isAdmin: false });

    // Function for clear local storage
    const unSetUser = () => {
        localStorage.clear();
    };

    useEffect(() => {
        console.log(user);
    }, [user]);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_URI}/users/profile`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
        })
            .then((response) => response.json())
            .then((data) => {
                console.log(data);
                setUser({ id: data._id, isAdmin: data.isAdmin });
            });
    }, []);

    return (
        <UserProvider value={{ user, setUser, unSetUser }}>
            <Router>
                <AppNavbar />
                <Routes>
                    <Route path="/" element={<Home />} />
                    <Route path="/courses" element={<Courses />} />
                    <Route path="/courses/:courseId" element={<CourseView />} />
                    <Route
                        path="/register"
                        element={!user.id ? <Register /> : <Navigate to="/" />}
                    />
                    <Route
                        path="/login"
                        element={!user.id ? <Login /> : <Navigate to="/" />}
                    />
                    <Route path="/logout" element={<Logout />} />
                    <Route path="*" element={<PageNotFound />} />
                </Routes>
            </Router>
        </UserProvider>
    );
}

export default App;

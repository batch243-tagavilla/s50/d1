import { useEffect, useState, useContext } from "react";
import { Row, Col, Button, Card } from "react-bootstrap";
import { Link } from "react-router-dom";
import UserContext from "../UserContext";

export default function CourseCard(props) {
    const { _id, description, price, slots } = props.courseProp;
    const { user } = useContext(UserContext);

    // Use the "useState" hook for this component to be able to store its state specifically to monitor the number of enrollees
    // State are used to keep track information related to individual components
    /* 
        Syntax: const [getter, setter] = useState("initialGetterValue");
    */

    const [enrollees, setEnrollees] = useState(0);
    const [slotsAvailable, setSlotsAvailable] = useState(slots);
    const [isAvailable, setIsAvailable] = useState(true);

    // Add a "useEffect" hook to have "CourseCard" component do perform a certain task after every DOM update.
    /* 
        Syntax: useEffect(functionToBeTriggered, [statesToBeMonitored])
    */

    useEffect(() => {
        // console.log(isAvailable);
        if (slotsAvailable === 0) {
            setIsAvailable(false);
            // console.log(isAvailable);
        }
    }, [slotsAvailable]);

    // Since enrollees is declared as a constant variable, directly reassigning the value is not allowed or it will cause an error
    // enrollees = 1;

    function enroll() {
        if (slotsAvailable === 1) {
            alert("Congrats");
        }

        setEnrollees(enrollees + 1);
        setSlotsAvailable(slotsAvailable - 1);
    }

    function notUserAlert() {
        alert(`Login first`);
    }

    return (
        <Row className="m-3">
            <Col xs={12} md={4} className="offset-md-4 offset-0">
                <Card className="p-4">
                    <Card.Body className="p-0">
                        <Card.Title>{props.courseProp.name}</Card.Title>

                        <Card.Subtitle>Description:</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>

                        <Card.Subtitle>Price:</Card.Subtitle>
                        <Card.Text>Php {price}</Card.Text>

                        <Card.Subtitle>Enrollees:</Card.Subtitle>
                        <Card.Text>{enrollees}</Card.Text>

                        <Card.Subtitle>Slots available:</Card.Subtitle>
                        <Card.Text>{slotsAvailable}</Card.Text>

                        {!user.id ? (
                            <Button
                                as={Link}
                                to="/login"
                                variant="primary"
                                onClick={notUserAlert}
                                disabled={!isAvailable}
                            >
                                Enroll
                            </Button>
                        ) : (
                            <Button
                                as={Link}
                                to={`/courses/${_id}`}
                                disabled={!isAvailable}
                            >
                                Details
                            </Button>
                        )}
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    );
}

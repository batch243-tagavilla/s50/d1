import { Container, Nav, Navbar } from "react-bootstrap";
import { Fragment, useContext } from "react";
import { NavLink } from "react-router-dom";
import UserContext from "../UserContext";

export default function AppNavbar() {
    const { user } = useContext(UserContext);

    return (
        // <Container>
            <Navbar variant="dark" bg="dark" expand="md" className="vw-100">
                <Container className="container-fluid">
                    <Navbar.Brand as={NavLink} to="/">
                        Course Booking
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav variant="pills" className="ms-auto">
                            <Nav.Link
                                as={NavLink}
                                to="/"
                                className="navbar-hover"
                            >
                                Home
                            </Nav.Link>
                            <Nav.Link
                                as={NavLink}
                                to="/courses"
                                className="navbar-hover"
                            >
                                Courses
                            </Nav.Link>

                            {user.id ? (
                                <Nav.Link
                                    as={NavLink}
                                    to="/logout"
                                    className="navbar-hover"
                                >
                                    Logout
                                </Nav.Link>
                            ) : (
                                <Fragment>
                                    <Nav.Link
                                        as={NavLink}
                                        to="/register"
                                        className="navbar-hover"
                                    >
                                        Register
                                    </Nav.Link>
                                    <Nav.Link
                                        as={NavLink}
                                        to="/login"
                                        className="navbar-hover"
                                    >
                                        Login
                                    </Nav.Link>{" "}
                                </Fragment>
                            )}
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        // </Container>
    );
}

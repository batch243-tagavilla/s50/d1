import { Row, Col, Card } from "react-bootstrap";

export default function Highlights() {
    return (
        <Row className="mt-3 mb-3">
            {/* First Card */}
            <Col xs={12} md={4}>
                <Card className="p-3 cardHighlight">
                    <Card.Body>
                        <Card.Title>
                            <h2>Learn from Home</h2>
                        </Card.Title>
                        <Card.Text>
                            Lorem ipsum dolor sit amet consectetur adipisicing
                            elit. Est enim nemo ea illo nobis, officia, deserunt
                            assumenda esse vitae iusto temporibus, iure
                            excepturi exercitationem qui corporis. Iure dolor
                            est itaque? Lorem ipsum, dolor sit amet consectetur
                            adipisicing elit. Sapiente deleniti autem quasi
                            tenetur quibusdam corrupti? Quos hic, architecto
                            ducimus minus officia dolor ex doloremque illum nam
                            voluptates illo asperiores vel. Lorem ipsum dolor
                            sit amet, consectetur adipisicing elit. Deserunt
                            corporis mollitia, laboriosam accusamus earum rerum
                            beatae aspernatur maxime sed totam amet dolor
                            aperiam nemo placeat, unde sit. Mollitia, tenetur.
                            Sequi?
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>

            {/* Second Card */}
            <Col xs={12} md={4}>
                <Card className="p-3 cardHighlight">
                    <Card.Body>
                        <Card.Title>
                            <h2>Study Now, Pay Later</h2>
                        </Card.Title>
                        <Card.Text>
                            Lorem ipsum dolor sit amet consectetur adipisicing
                            elit. Est enim nemo ea illo nobis, officia, deserunt
                            assumenda esse vitae iusto temporibus, iure
                            excepturi exercitationem qui corporis. Iure dolor
                            est itaque? Lorem ipsum, dolor sit amet consectetur
                            adipisicing elit. Sapiente deleniti autem quasi
                            tenetur quibusdam corrupti? Quos hic, architecto
                            ducimus minus officia dolor ex doloremque illum nam
                            voluptates illo asperiores vel. Lorem ipsum dolor
                            sit amet, consectetur adipisicing elit. Deserunt
                            corporis mollitia, laboriosam accusamus earum rerum
                            beatae aspernatur maxime sed totam amet dolor
                            aperiam nemo placeat, unde sit. Mollitia, tenetur.
                            Sequi?
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>

            {/* Third Card */}
            <Col xs={12} md={4}>
                <Card className="p-3 cardHighlight">
                    <Card.Body>
                        <Card.Title>
                            <h2>Be part of our Community</h2>
                        </Card.Title>
                        <Card.Text>
                            Lorem ipsum dolor sit amet consectetur adipisicing
                            elit. Est enim nemo ea illo nobis, officia, deserunt
                            assumenda esse vitae iusto temporibus, iure
                            excepturi exercitationem qui corporis. Iure dolor
                            est itaque? Lorem ipsum, dolor sit amet consectetur
                            adipisicing elit. Sapiente deleniti autem quasi
                            tenetur quibusdam corrupti? Quos hic, architecto
                            ducimus minus officia dolor ex doloremque illum nam
                            voluptates illo asperiores vel. Lorem ipsum dolor
                            sit amet, consectetur adipisicing elit. Deserunt
                            corporis mollitia, laboriosam accusamus earum rerum
                            beatae aspernatur maxime sed totam amet dolor
                            aperiam nemo placeat, unde sit. Mollitia, tenetur.
                            Sequi?
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    );
}
